import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import About from './About'
import Contact from './Contact'
import Team from './Team'
import './index.css'
import { BrowserRouter, Routes, Route} from 'react-router-dom'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<App />}></Route>
        <Route path='/about' element={<About />}></Route>
        <Route path='/contact' element={<Contact />}></Route>
        <Route path='/team' element={<Team />}></Route>
      </Routes>
    </BrowserRouter>
  </React.StrictMode>,
)
